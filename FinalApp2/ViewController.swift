//
//  ViewController.swift
//  FinalApp
//
//  Created by Nurba on 05.03.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var openButton: UIButton!
    
    var arr =  [Title]()
    
    var delegate : CollectionViewCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: blockCell, bundle: nil), forCellWithReuseIdentifier: blockCell)
        view.addSubview(collectionView)
        
        arr.append(Title(name: "Do "))
        arr.append(Title(name: "Build "))
        arr.append(Title(name: "Destroy "))
        arr.append(Title(name: "Crash "))
        arr.append(Title(name: "Kill "))
        arr.append(Title(name: "Type"))
        
    }
    
    var blockCell = "CollectionViewCell"
    
    
    @IBAction func tapOpen(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "table") as! TableViewController
        
        vc.delegate = self
        vc.action = arr.first?.name
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let block = collectionView.dequeueReusableCell(withReuseIdentifier: blockCell , for: indexPath) as! CollectionViewCell
        let item = arr[indexPath.row]
        block.delegate = self
        block.collectLabel.text = item.name
        
        
        return block
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "table") as! TableViewController
        
        vc.delegate = self
        vc.action = arr[indexPath.row].name
        navigationController?.pushViewController(vc, animated: true)
    }
    
  
    
}

extension ViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 420, height: 70)
    }
    
 
}
