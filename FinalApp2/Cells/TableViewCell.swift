//
//  TableViewCell.swift
//  FinalApp2
//
//  Created by Nurba on 05.03.2021.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var tableLabel: UILabel!
    
    var delegate:TableViewController?

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
